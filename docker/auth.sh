#!/bin/bash

php bin/console doctrine:migrations:migrate -n
php bin/console doctrine:fixtures:load --append

echo '##### JWT' &&
cp -r /jwt /app/config/jwt

symfony server:start
