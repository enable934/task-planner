#!/bin/bash

php bin/console doctrine:migrations:migrate -n &&
php bin/console doctrine:fixtures:load --append

php-fpm
