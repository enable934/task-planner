import React from 'react'

import {Row, Col, notification, Spin, Button} from "antd";

import {locale} from 'devextreme/localization';
import Scheduler, {View} from 'devextreme-react/scheduler';
import ColorBox from "devextreme/ui/color_box";

import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import Notification from "../../components/Notification";
import {getRaw, postRaw, patchRaw, deleteRaw} from "../../services/crud";
import moment from "moment";
import {DeleteOutlined} from "@ant-design/icons";
import {connect} from "react-redux";

locale(navigator.language);

class Home extends React.Component {
  state = {
    appointments: [],
    loading: true,
  }

  componentDidMount() {
    this.getAppointments();
  }

  constructor(props) {
    super(props);
    this.notificationModal = React.createRef();
    this.schedulerRef = React.createRef();
  }

  get scheduler() {
    return this.schedulerRef.current.instance;
  }

  getAppointments = async () => {
    this.setState({loading: true});
    const result = await getRaw('/api/task');

    if (result.status !== false) {
      const {data} = result;
      data.map(a => {
        a.startDate = moment(a.startDate).toDate();
        a.endDate = moment(a.endDate).toDate();
        a.notifications.map(n => {
          n.date = moment(n.date).toDate();

          return n;
        })

        return a;
      });
      this.setState({
        appointments: data
      });
    } else {
      notification.error({
        message: 'Error',
        description: result.message,
      });
    }
    this.setState({loading: false});
  }

  createAppointment = async (data) => {
    data.notifications.map(n => {
      n.date = moment(n.date).format();
      return n;
    })
    const result = await postRaw(
      '/api/task',
      {
        name: data.name,
        startDate: moment(data.startDate).format(),
        endDate: moment(data.endDate).format(),
        description: data.description,
        notifications: data.notifications,
        color: data.color
      }
    );

    if (result.status !== false) {
      notification.info({
        message: 'Task',
        description: 'New task created',
      });
    } else {
      notification.error({
        message: 'Error',
        description: result.message,
      });
    }

  }

  editAppointment = async (data) => {
    data.notifications.map(n => {
      n.date = moment(n.date).format();
      return n;
    })
    const result = await patchRaw(
      '/api/task/' + data.guid,
      {
        name: data.name,
        startDate: moment(data.startDate).format(),
        endDate: moment(data.endDate).format(),
        description: data.description,
        notifications: data.notifications,
        color: data.color
      }
    );

    if (result.status !== false) {
      notification.info({
        message: 'Updating',
        description: 'Task updated successful',
      });
    } else {
      notification.error({
        message: 'Error',
        description: result.message,
      });
    }
  }
  deleteAppointment = async (data) => {
    const result = await deleteRaw('/api/task/' + data.guid);

    if (result.status !== false) {
      notification.info({
        message: 'Deleting',
        description: 'Task deleted successful',
      });
    } else {
      notification.error({
        message: 'Error',
        description: result.message,
      });
    }
  }

  onAddingNewAppointment = async (data) => {
    this.setState({loading: true});
    await this.createAppointment(data.appointmentData);
    await this.getAppointments();
    this.setState({loading: false});
  }

  onEditAppointment = async (e) => {
    this.setState({loading: true});
    await this.editAppointment(e.newData);
    await this.getAppointments();
    this.setState({loading: false});
  }

  onDeletingAppointment = async (e) => {
    this.setState({loading: true});
    await this.deleteAppointment(e.appointmentData);
    await this.getAppointments();
    this.setState({loading: false});
  }

  onAppointmentFormOpening(data, ref, user) {
    let form = data.form;
    let isEdit = true;
    if (data.appointmentData.guid === undefined) {
      isEdit = true;
      data.appointmentData.notifications = [];
    }

    form.option('items', [
      {
        label: {
          text: 'Name'
        },
        isRequired: true,
        editorType: 'dxTextBox',
        dataField: 'name',
        editorOptions: {
          width: '100%',
          value: data.appointmentData.name
        }
      },
      {
        isRequired: true,
        editorType: 'dxColorBox',
        dataField: 'color',
        editorOptions: {
          width: '100%',
          value: data.appointmentData.color ?? '#337ab7',
          placeholder: 'choose color of task'
        }
      },
      {
        dataField: 'startDate',
        editorType: 'dxDateBox',
        editorOptions: {
          width: '100%',
          type: 'datetime'
        }
      },
      {
        name: 'endDate',
        dataField: 'endDate',
        editorType: 'dxDateBox',
        editorOptions: {
          width: '100%',
          type: 'datetime',
        }
      },
      {
        label: {
          text: 'Description'
        },
        editorType: 'dxTextArea',
        dataField: 'description',
        editorOptions: {
          width: '100%',
          value: data.appointmentData.description
        }
      },
      {
        editorType: 'dxNumberBox',
        label: {
          text: 'Added notifications'
        },
        editorOptions: {
          readOnly: true,
          value: data.appointmentData.notifications ? data.appointmentData.notifications.length : 0,
        }
      },
      {
        editorType: 'dxTagBox',
        dataField: 'notifications',
        editorOptions: {
          value: data.appointmentData.notifications ?? [],
        },
        visible: false
      },
      {
        name: 'button',
        itemType: 'button',
        buttonOptions: {
          text: 'Edit notifications',
          onClick: () => {
            ref.current.showModal(data.appointmentData, isEdit);
          }
        },
        visible: user.chatId!==null
      }
    ]);
  }

  render() {
    const {appointments, loading} = this.state;
    const {user} = this.props;
    return (
      <div>
        <Row gutter={[10, 10]}>
          <Col xl={24} xs={24}>
            <div id="scheduler" style={{marginTop: 10}}>
              <Spin spinning={loading} tip={"loading.."}>
                <Notification ref={this.notificationModal} visible={false}/>
                <Scheduler
                  ref={this.schedulerRef}
                  dataSource={appointments}
                  groupByDate={true}
                  defaultCurrentView="week"
                  height={1250}
                  showCurrentTimeIndicator={true}
                  locale="ru-RU"
                  currentView="week"
                  showAllDayPanel={true}
                  onAppointmentFormOpening={(data) => {
                    this.onAppointmentFormOpening(data, this.notificationModal,user)
                  }}
                  onAppointmentUpdating={this.onEditAppointment}
                  onAppointmentAdding={this.onAddingNewAppointment}
                  onAppointmentRendered={(e) => e.appointmentElement.style.backgroundColor = e.appointmentData.color}
                  onAppointmentDeleting={this.onDeletingAppointment}
                  appointmentTooltipRender={(model) =>
                    (<div className="dx-tooltip-appointment-item">
                      <div className="dx-tooltip-appointment-item-marker">
                        <div className="dx-tooltip-appointment-item-marker-body"
                             style={{backgroundColor: model.appointmentData.color}}/>
                      </div>
                      <div className="dx-tooltip-appointment-item-content">
                        <div
                          className="dx-tooltip-appointment-item-content-subject">{model.appointmentData.name}</div>
                        <div
                          className="dx-tooltip-appointment-item-content-date">{moment(model.appointmentData.startDate).format('hh:mm a')} - {moment(model.appointmentData.endDate).format('hh:mm a')} </div>
                      </div>
                      <div className="dx-tooltip-appointment-item-delete-button-container">
                        <div
                          className="dx-tooltip-appointment-item-delete-button dx-button dx-button-normal dx-button-mode-text dx-widget dx-button-has-icon"
                          aria-label="trash" tabIndex="0" role="button">
                          <div className="dx-button-content">
                            <Button icon={<DeleteOutlined/>} onClick={() => {
                              this.scheduler.deleteAppointment(model.appointmentData);
                            }}/>
                          </div>
                        </div>
                      </div>
                    </div>)
                  }
                >
                  <View
                    type="day"
                    intervalCount={7}
                    name="Week"
                  />
                </Scheduler>
              </Spin>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
export default connect(
  ({user}) => ({user})
)(Home)
