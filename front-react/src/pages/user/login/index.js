import React from 'react'
import {Form, Input, Button, notification} from 'antd'
import {Helmet} from 'react-helmet'
import {Link} from 'react-router-dom'
import {login} from 'services/user';
import styles from './style.module.scss'
import {connect} from "react-redux";
import {LockOutlined, LoginOutlined, UserOutlined} from "@ant-design/icons";

class Login extends React.Component {
  formRef = React.createRef();

  state = {
    loading: false,
  };

  onFinish = async (data) => {
    this.setState({ loading: true});
    const result = await login(data);
    this.setState({ loading: false});
    if(result.status !== false) {
      const {dispatch} = this.props;
      dispatch({
        type: 'user/LOAD_CURRENT_ACCOUNT',
      })
    } else {
      notification.warning({
        message: 'Log in',
        description: result.message ?? 'Cannot log in',
      })
    }
  };

  render() {
    const {loading} = this.state;
    return (
      <div>
        <Helmet title="Log in"/>
        <div className={styles.block}>
          <div className="row">
            <div className="col-xl-12">
              <div className={styles.inner}>
                <div className={styles.form}>
                  <div className="text-center">
                    <Link to="/">
                      <img src="/resources/images/logo.png" alt="" style={{width:174}} />
                    </Link>
                  </div>
                  <br/>
                  <Form layout="vertical" ref={this.formRef} onFinish={this.onFinish}>
                    <Form.Item name="username" rules={[{required: true, message: 'Please enter username'}]}>
                      <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" size="default"/>
                    </Form.Item>
                    <Form.Item name="password" rules={[{required: true, message: 'Please enter password'}]}>
                      <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" size="default" type="password"/>
                    </Form.Item>
                    <div className="form-actions">
                      <Button
                        loading={loading}
                        type="primary"
                        className="width-150 mr-4"
                        htmlType="submit"
                        icon={<LoginOutlined />}
                        name="log in"
                      >
                        Log in
                      </Button>
                      <Link to="/register">&rarr; register now</Link>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default connect(
  ({user}) => ({user})
)(Login)
