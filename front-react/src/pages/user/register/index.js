import React from 'react'
import {Form, Input, Button, notification} from 'antd'
import {Helmet} from 'react-helmet'
import {Link} from 'react-router-dom'
import {register} from 'services/user';
import styles from './style.module.scss'
import {connect} from "react-redux";
import {LoginOutlined, UserOutlined, LockOutlined} from "@ant-design/icons";

class Login extends React.Component {
  formRef = React.createRef();

  state = {
    loading: false,
  };

  onFinish = async (data) => {
    this.setState({ loading: true});
    const result = await register(data);
    this.setState({ loading: false});
    if(result.status !== false) {
      const {dispatch} = this.props;
      dispatch({
        type: 'user/LOAD_CURRENT_ACCOUNT',
      });
      window.location = '/';
    } else {
      notification.warning({
        message: 'Log in',
        description: result.message ?? 'Cannot log in',
      })
    }
  };

  render() {
    const {loading} = this.state;
    return (
      <div>
        <Helmet title="Log in"/>
        <div className={styles.block}>
          <div className="row">
            <div className="col-xl-12">
              <div className={styles.inner}>
                <div className={styles.form}>
                  <div className="text-center">
                    <Link to="/">
                      <img src="/resources/images/logo.png" alt="" style={{width:174}} />
                    </Link>
                  </div>
                  <br/>
                  <h1>Registration</h1>
                  <Form layout="vertical" ref={this.formRef} onFinish={this.onFinish}>
                    <Form.Item
                      name="username"
                      label="Username"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your username!',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      name="password"
                      label="Password"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your password!',
                        },
                      ]}
                      hasFeedback
                    >
                      <Input.Password />
                    </Form.Item>

                    <Form.Item
                      name="confirm"
                      label="Confirm Password"
                      dependencies={['password']}
                      hasFeedback
                      rules={[
                        {
                          required: true,
                          message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                          validator(rule, value) {
                            if (!value || getFieldValue('password') === value) {
                              return Promise.resolve();
                            }
                            return Promise.reject('The two passwords that you entered do not match!');
                          },
                        }),
                      ]}
                    >
                      <Input.Password />
                    </Form.Item>
                    <div className="form-actions">
                      <Button
                        loading={loading}
                        type="primary"
                        className="login-form-button mr-4"
                        htmlType="submit"
                        icon={<LoginOutlined />}
                        name="register"
                      >
                        Register
                      </Button>
                      <Link to="/login">&larr; back to login</Link>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default connect(
  ({user}) => ({user})
)(Login)
