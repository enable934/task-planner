import React from 'react'
import {Modal, List, DatePicker, notification} from 'antd';
import moment from "moment/moment";
import './style.css'
import {postRaw, putRaw} from "../../services/crud";

export default class Notification extends React.Component {
  state = {
    visible: false,
    appointment: {},
    initLoading: true,
    loading: false,
    notifications: [],
    isEdit: false
  };

  componentDidMount() {
    const {visible} = this.props;
    this.setState({visible, initLoading: false, notifications: []});
  }

  showModal = (appointment, isEdit) => {
    this.setState({
      visible: true,
      isEdit,
      appointment: appointment,
      notifications: appointment.notifications
    });
    appointment.notifications = this.state.notifications;
  };

  handleOk = e => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  disabledDate = (current) => {
    const {appointment} = this.state;
    return current && (current < moment().subtract(1,'days') || current > moment(appointment.endDate).add(1, 'days'));
  }

  onDateChange = async (date, dateString, item) => {
    const {notifications, isEdit} = this.state;
    this.setState({initLoading: true})
    const index = notifications.indexOf(item);
    if (!isEdit) {
      if (index !== -1) {
        notifications[index] = {guid: null, date: date.toDate()};
      }
    } else {
      const result = await this.updateNotification(item, date);
      if (index !== -1) {
        notifications[index] = {guid: result.guid, date: moment(result.date, 'DD.MM.YYYY HH:mm').toDate()};
      }
    }
    this.setState({notifications, initLoading: false});
  }

  onNewDatePicked = async (date) => {
    const {notifications, isEdit} = this.state;
    this.setState({initLoading: true})
    if (!isEdit) {
      notifications.push({guid: null, date: date.toDate()});
    } else {
      const result = await this.addNotification(date.format());
      notifications.push({guid: result.guid, date: moment(result.date, 'DD.MM.YYYY HH:mm').toDate()});
    }
    this.setState({notifications, initLoading: false});
  }

  updateNotification = async (item, date) => {
    const result = await putRaw(
      '/api/notification/' + item.guid,
      {date: date.format()}
    );

    if (result.status !== false) {
      notification.info({
        message: 'Notification',
        description: 'Notification updated',
      });
      return result.data;
    } else {
      notification.error({
        message: 'Error',
        description: result.message,
      });
    }
  }

  addNotification = async (date) => {
    const {appointment} = this.state;
    const result = await postRaw(
      '/api/task/' + appointment.guid + '/notification',
      {date: date}
    );

    if (result.status !== false) {
      notification.info({
        message: 'Notification',
        description: 'New notification added',
      });
      return result.data;
    } else {
      notification.error({
        message: 'Error',
        description: result.message,
      });
    }
  }

  render() {
    const {appointment, initLoading, notifications} = this.state;
    return (
      <>
        <Modal
          title={"Edit notifications for task " + (appointment === undefined ? appointment.name : 'NEW')}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          zIndex={9999}
        >
          <List
            className="demo-loadmore-list"
            loading={initLoading}
            itemLayout="horizontal"
            dataSource={notifications}
            renderItem={(item) => (
              <List.Item
                actions={[<a key="list-loadmore-edit">
                </a>]}
              >
                <List.Item.Meta
                  title={
                    <DatePicker
                      bordered={false}
                      size={"small"}
                      placeholder={"edit date"}
                      onChange={(date, dateString) => {
                        this.onDateChange(date, dateString, item)
                      }}
                      allowClear={false}
                      disabledDate={this.disabledDate}
                      showTime={true}
                      className={"notification-date-picker"}
                      value={moment(item.date)}
                    />}
                  description={moment(item.date).fromNow()}
                />
              </List.Item>
            )}
          />
          <DatePicker
            placeholder={"Add new notification"}
            onOk={(date) => {
              this.onNewDatePicked(date)
            }}
            disabledDate={this.disabledDate}
            showTime={true}
            className={"notification-date-picker"}
          />
        </Modal>
      </>
    );
  }
}
