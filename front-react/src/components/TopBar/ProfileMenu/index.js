import React from 'react'
import {connect} from 'react-redux'
// import {Link} from 'react-router-dom'
import {Menu, Dropdown, Avatar, Button, notification} from 'antd'
import {UserOutlined} from '@ant-design/icons';
import {getRaw} from 'services/crud'
import styles from './style.module.scss'
import {api} from "../../../services/api";

class ProfileMenu extends React.Component {

  state = {
    href: '',
  }

  constructor(props) {
    super(props);
    this.getTelegramLink();
  }

  componentDidMount() {
    this.setState({href:''});
  }

  getTelegramLink = async () => {
    const response = await api('post', '/api/telegram-link');
    const {status, data} = response;
    if (status !== false) {
      this.setState({href: data.link});
    } else {
      notification.info({
        message: 'Cannot get telegram link'
      })
    }
  }

  logout = () => {
    const {dispatch} = this.props
    dispatch({
      type: 'user/LOGOUT',
    })
  }

  render() {
    const {user} = this.props;
    const {href} = this.state;

    const menu = (
      <Menu selectable={false}>
        <Menu.Item>
          Your username: <strong>{user.username}</strong>
        </Menu.Item>
        <Menu.Divider/>
        <Menu.Item>
          <div>
            Your telegram chat id: <strong>{user.chatId || '-'}</strong>
          </div>
        </Menu.Item>
        <Menu.Item>
          <div>
            <Button href={href} target="_blank" type="link">Link Telegram</Button>
          </div>
        </Menu.Item>
        <Menu.Divider/>
        <Menu.Item>
          <a id="logout" href="!#" onClick={this.logout}>
            <i className={`${styles.menuIcon} icmn-exit`}/>
            Log out
          </a>
        </Menu.Item>
      </Menu>
    )

    const A = <Avatar className={styles.avatar} shape="square" size="large" icon={<UserOutlined/>}/>

    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className={styles.dropdown}>
          {A}
        </div>
      </Dropdown>
    )
  }
}

export default connect(
  ({user}) => ({user})
)(ProfileMenu)
