import decode from "jwt-decode";
import { api } from './api';

const getToken = () => {
  return localStorage.getItem('token');
};

const setToken = (token) => {
  localStorage.setItem('token', token);
};

const deleteToken = () => {
  localStorage.removeItem('token');
};

const isTokenExpired = (token) => {
  try {
    const decoded = decode(token);
    return (decoded.exp < Date.now() / 1000);
  }
  catch (err) {
    return false;
  }
};

const loggedIn = () => {
  const token = getToken();
  return !!token && !isTokenExpired(token);
};

export async function currentUser () {
  if (loggedIn) {
    const token = getToken();
    if (!token) {
      return false;
    }
    try {
      const response = await api('get', '/api/current-user');
      const { status, data } = response;
      if (status !== false) {
        return data;
      } else {
        logout();
        return false;
      }
    } catch (e) {
      logout();
      return false;
    }
  } else {
    return false;
  }
}

export async function login (params) {
  try {
    const response = await api('post', '/api/authorization', params);
    const { status, message, data } = response;
    if (status !== false) {
      setToken(data.token);
      return {
        status: true,
        message: message ?? false
      };
    } else {
      return {
        status: false,
        message: message ?? false
      };
    }
  } catch (e) {
    return {
      status: false,
      message: e
    };
  }
}

export async function register (params) {
  try {
    const response = await api('post', '/api/register', params);
    const { status, message, data } = response;
    if (status !== false) {
      setToken(data.token);
      return {
        status: true,
        message: message ?? false
      };
    } else {
      return {
        status: false,
        message: message ?? false
      };
    }
  } catch (e) {
    return {
      status: false,
      message: e
    };
  }
}

export function logout () {
  deleteToken();
}
