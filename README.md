## 1. Встановлення docker
Інструкцію про встановлення docker можно знайти тут: https://docs.docker.com/engine/install/
## 2. Запуск проєкту
Для запуску в корені проєкту прописати:
```shell script
docker-compose up -d
```
Для зупинки в корені проєкту прописати:
```shell script
docker-compose down
```
## 3. Налаштування PhpStorm
> Примітка: PhpStorm має бути версії 2020.2 і вище
1. В **Settings > PHP** додаємо новий CLI інтерпретатор. Вибираємо пункт **docker-compose**.
   **service** вибираємо **cds_oks**. **Lifecycle** обов'язково вибираємо **connect to existing container**.
2. В **Settings > Languages & Frameworks > PHP > Test Frameworks** додаємо **PhpUnit by remote interpreter**. Вибираємо **cds_oks**.
   **Path to phpunit.phar**: **back/bin/phpunit**.
   **Default configuration file**: **back/phpunit.xml.dist**
3. Після цього запуститься 3 мікросервіси, 3 бази даних і фронтенд  