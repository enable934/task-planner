<?php

declare(strict_types=1);

namespace App\Tests;

use App\Dto\InternalResult;
use App\Service\AuthApiService;

/**
 * @internal
 * @coversNothing
 */
class AuthTest extends IntegrationTestCase
{
    public function testAuth(): string
    {
        $mocked = $this->createMock(AuthApiService::class);
        $internalResult = new InternalResult();
        $internalResult->setData(['token' => '123']);
        $mocked
            ->method('auth')
            ->willReturn($internalResult)
        ;
        self::$container->set('test'.AuthApiService::class, $mocked);

        /** @var AuthApiService $authService */
        $authService = self::$container->get('test'.AuthApiService::class);
        $result = $authService->auth('user', '123');

        $this->assertTrue($result->isStatus());
        $this->assertArrayHasKey('token', $result->getData());

        return $result->getData()['token'];
    }

    /**
     * @depends testAuth
     */
    public function testGetUser(string $token): void
    {
        $mocked = $this->createMock(AuthApiService::class);
        $internalResult2 = new InternalResult();
        $internalResult2->setData(['chatId' => 123, 'guid' => '123']);
        $mocked
            ->method('getUser')
            ->willReturn($internalResult2)
        ;
        self::$container->set('test'.AuthApiService::class, $mocked);

        /** @var AuthApiService $authService */
        $authService = self::$container->get('test'.AuthApiService::class);
        $result = $authService->getUser($token);

        $this->assertTrue($result->isStatus());
        $this->assertArrayHasKey('chatId', $result->getData());
        $this->assertArrayHasKey('guid', $result->getData());
    }
}
