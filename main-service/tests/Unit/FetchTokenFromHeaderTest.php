<?php

declare(strict_types=1);

use App\Traits\FetchTokenFromHeaderTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 * @coversNothing
 */
class FetchTokenFromHeaderTest extends TestCase
{
    use FetchTokenFromHeaderTrait;

    public function testProperFetching()
    {
        $request = new Request();
        $request->headers->add(['Authorization' => 'Bearer test']);
        $token = $this->fetchTokenFromRequest($request);

        $this->assertSame('test', $token);
    }

    public function testFetchingWhereTokenNotExists()
    {
        $request = new Request();
        $token = $this->fetchTokenFromRequest($request);

        $this->assertSame('', $token);
        $this->assertEmpty($token);
    }
}
