<?php

declare(strict_types=1);

use App\Dto\InternalResult;
use App\Service\AuthApiService;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class AuthApiServiceTest extends TestCase
{
    private AuthApiService $authService;

    protected function setUp()
    {
        $this->authService = new AuthApiService('http://test');
    }

    public function testAuth()
    {
        $expected = new InternalResult();
        $expected->setBadMessage('No connection with auth microservice');
        $result = $this->authService->auth('123', '123');

        $this->assertSame($expected->getMessage(), $result->getMessage());
    }

    public function testRegister()
    {
        $expected = new InternalResult();
        $expected->setBadMessage('No connection with auth microservice');
        $result = $this->authService->register('123', '123');

        $this->assertSame($expected->getMessage(), $result->getMessage());
    }

    public function testGetUser()
    {
        $expected = new InternalResult();
        $expected->setBadMessage('No connection with auth microservice');
        $result = $this->authService->getUser('123');

        $this->assertSame($expected->getMessage(), $result->getMessage());
    }
}
