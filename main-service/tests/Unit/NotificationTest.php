<?php

declare(strict_types=1);

use App\Entity\Notification;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class NotificationTest extends TestCase
{
    public function testValidJsonEncode()
    {
        $notification = new Notification();
        $result = json_encode($notification);

        $this->assertJson($result);

        $resultDecoded = json_decode($result, true);

        $this->assertNotEmpty($resultDecoded['date']);
        $this->assertNotEmpty($resultDecoded['guid']);
    }

    public function testValidJsonEncodeDateInAtomFormat()
    {
        $notification = new Notification();
        $notification->setDate(new DateTimeImmutable('12.12.2012'));
        $result = json_encode($notification);

        $this->assertJson($result);

        $resultDecoded = json_decode($result, true);

        $this->assertNotEmpty($resultDecoded['date']);
        $this->assertNotEmpty($resultDecoded['guid']);
        $this->assertSame('2012-12-12T00:00:00+00:00', $resultDecoded['date']);
    }
}
