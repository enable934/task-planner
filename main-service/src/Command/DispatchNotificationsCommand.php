<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Notification;
use App\Message\NotificationMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;

class DispatchNotificationsCommand extends Command
{
    protected static $defaultName = 'app:dispatch';

    private EntityManagerInterface $entityManager;

    private MessageBusInterface $bus;

    public function __construct(EntityManagerInterface $entityManager, MessageBusInterface $bus, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->bus = $bus;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Notification[] $notifications */
        $notifications = $this->entityManager->getRepository(Notification::class)->findBy(['sent' => false]);

        foreach ($notifications as $notification) {
            $date = $notification->getDate();
            $currentDate = new \DateTimeImmutable();

            if ($date->getTimestamp() > $currentDate->getTimestamp()) {
                $delay = $date->getTimestamp() - $currentDate->getTimestamp();
                $this->bus->dispatch(new NotificationMessage(
                    $notification->getTask()->getName(),
                    $notification->getTask()->getDescription(),
                    $notification->getChatId()
                ), [new DelayStamp($delay * 1000)]);
                $output->writeln(sprintf('Message %s dispatched for date \'%s\'', $notification->getGuid(), $notification->getDate()->format('d.m.Y H:i Z')));
                $notification->setSent(true);
            }
        }
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
