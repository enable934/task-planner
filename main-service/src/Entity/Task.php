<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 */
class Task implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     */
    private \DateTimeImmutable $startDate;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     */
    private \DateTimeImmutable $endDate;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $description = '';

    /**
     * @ORM\Column(type="string")
     */
    private string $externalGuid;

    /**
     * @ORM\Column(type="string")
     */
    private string $guid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="task", cascade={"remove"})
     */
    private Collection $notifications;

    /**
     * @ORM\Column(type="string", options={"default": "#337ab7"})
     */
    private string $color = '#337ab7';

    public function __construct()
    {
        $this->notifications = new ArrayCollection();
        $this->guid = Uuid::uuid4()->toString();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExternalGuid(): string
    {
        return $this->externalGuid;
    }

    public function setExternalGuid(string $externalGuid): self
    {
        $this->externalGuid = $externalGuid;

        return $this;
    }

    /**
     * @return ArrayCollection|Notification[]
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    public function setNotifications(Collection $notifications): self
    {
        $this->notifications = $notifications;

        return $this;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
            $notification->setTask($this);
        }

        return $this;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'guid' => $this->getGuid(),
            'name' => $this->getName(),
            'startDate' => $this->getStartDate()->format(DATE_ATOM),
            'endDate' => $this->getEndDate()->format(DATE_ATOM),
            'description' => $this->getDescription(),
            'notifications' => $this->getNotifications()->toArray(),
            'color' => $this->getColor(),
        ];
    }
}
