<?php

declare(strict_types=1);

namespace App\Messenger\Serializer;

use App\Message\NotificationMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

class JsonMessageSerializer implements SerializerInterface
{
    public function decode(array $encodedEnvelope): Envelope
    {
        return new Envelope(new class() {
        });
    }

    public function encode(Envelope $envelope): array
    {
        $message = $envelope->getMessage();
        $allStamps = [];

        foreach ($envelope->all() as $stamps) {
            $allStamps = array_merge($allStamps, $stamps);
        }

        if ($message instanceof NotificationMessage) {
            $type = 'notification';

            return [
                'body' => json_encode($message),
                'headers' => [
                    'type' => $type,
                    'stamps' => serialize($allStamps),
                ],
            ];
        }

        return [];
    }
}
