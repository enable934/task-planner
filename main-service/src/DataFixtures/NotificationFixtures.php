<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Notification;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class NotificationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var Task $task1 */
        $task1 = $this->getReference(TaskFixtures::TASK1_REFERENCE);
        $task2 = $this->getReference(TaskFixtures::TASK1_REFERENCE);
        $notification = new Notification();
        $notification
            ->setDate(new \DateTimeImmutable('+6 hour'))
            ->setChatId(123)
        ;
        $task1->addNotification($notification);

        $notification2 = new Notification();
        $notification2
            ->setDate(new \DateTimeImmutable('+12 hour'))
            ->setChatId(123)
        ;
        $task1->addNotification($notification2);

        $notification3 = new Notification();
        $notification3
            ->setDate(new \DateTimeImmutable('+1 hour'))
            ->setChatId(123)
        ;
        $task2->addNotification($notification3);

        $manager->persist($notification);
        $manager->persist($notification2);
        $manager->persist($notification3);
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            TaskFixtures::class,
        ];
    }
}
