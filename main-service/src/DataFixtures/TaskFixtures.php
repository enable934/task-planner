<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TaskFixtures extends Fixture
{
    public const TASK1_REFERENCE = 'task1';
    public const TASK2_REFERENCE = 'task2';

    public function load(ObjectManager $manager)
    {
        $task = new Task();
        $task->setName('test')
            ->setStartDate(new \DateTimeImmutable())
            ->setEndDate(new \DateTimeImmutable('+1 day'))
            ->setDescription('test description')
            ->setExternalGuid('test')
        ;
        $task2 = new Task();
        $task2->setName('test2')
            ->setStartDate(new \DateTimeImmutable())
            ->setEndDate(new \DateTimeImmutable('+5 day'))
            ->setDescription('test2 description')
            ->setExternalGuid('test2')
        ;
        $manager->persist($task);
        $manager->persist($task2);
        $manager->flush();

        $this->addReference(self::TASK1_REFERENCE, $task);
        $this->addReference(self::TASK2_REFERENCE, $task2);
    }
}
