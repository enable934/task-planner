<?php

declare(strict_types=1);

namespace App\Dto;

class InternalResult implements \JsonSerializable
{
    protected array $data = [];
    protected bool $status = true;
    protected string $message = '';

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function isStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function setBadMessage(string $message): self
    {
        $this->message = $message;
        $this->status = false;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'data' => $this->data,
            'status' => $this->status,
            'message' => $this->message,
        ];
    }
}
