<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\InternalResult;
use App\Service\AuthApiService;
use App\Traits\FetchTokenFromHeaderTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    use FetchTokenFromHeaderTrait;

    private AuthApiService $authApiService;

    public function __construct(AuthApiService $authApiService)
    {
        $this->authApiService = $authApiService;
    }

    /**
     * @Route(path="/api/authorization", methods={"POST"})
     */
    public function login(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $result = $this->authApiService->auth($data['username'], $data['password']);

        return new JsonResponse($result);
    }

    /**
     * @Route(path="/api/current-user")
     */
    public function getCurrentUserInfo(Request $request): JsonResponse
    {
        $token = $this->fetchTokenFromRequest($request);
        $result = $this->authApiService->getUser($token);

        return new JsonResponse($result);
    }

    /**
     * @Route(path="/api/register")
     */
    public function register(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $result = $this->authApiService->register($data['username'], $data['password']);

        return new JsonResponse($result);
    }

    /**
     * @Route(path="/api/telegram-link")
     */
    public function linkTelegram(Request $request, AuthApiService $authApiService): JsonResponse
    {
        $result = new InternalResult();
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $userGuid = $resultOfAuth->getData()['guid'];
        $link = sprintf('https://www.telegram.me/%s?start=%s', $_ENV['BOT_USERNAME'], $userGuid);
        $result->setData(['link' => $link]);

        return new JsonResponse($result);
    }
}
