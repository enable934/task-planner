<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\InternalResult;
use App\Entity\Notification;
use App\Entity\Task;
use App\Service\AuthApiService;
use App\Traits\FetchTokenFromHeaderTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    use FetchTokenFromHeaderTrait;

    /**
     * @Route(path="/api/task", methods={"GET"})
     */
    public function getTaskList(Request $request, AuthApiService $authApiService, EntityManagerInterface $entityManager): JsonResponse
    {
        $result = new InternalResult();
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $userGuid = $resultOfAuth->getData()['guid'];
        $tasks = $entityManager->getRepository(Task::class)->findBy(['externalGuid' => $userGuid]);

        return new JsonResponse($result->setData($tasks));
    }

    /**
     * @Route(path="/api/task", methods={"POST"})
     */
    public function createTaskAndNotifications(Request $request, AuthApiService $authApiService, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $result = new InternalResult();

        if (!isset($data['name'])) {
            return new JsonResponse($result->setBadMessage('Name must be provided'));
        }

        if (!isset($data['startDate']) or !isset($data['endDate'])) {
            return new JsonResponse($result->setBadMessage('Start and End Dates must be provided'));
        }

        if (!isset($data['color'])) {
            return new JsonResponse($result->setBadMessage('Color must be provided'));
        }
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $userGuid = $resultOfAuth->getData()['guid'];
        $userChatId = $resultOfAuth->getData()['chatId'];
        $task = new Task();
        $task->setStartDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $data['startDate']))
            ->setEndDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $data['endDate']))
            ->setExternalGuid($userGuid)
            ->setName($data['name'])
            ->setColor($data['color'])
        ;

        if (isset($data['description'])) {
            $task->setDescription($data['description']);
        }

        if (null !== $userChatId) {
            foreach ($data['notifications'] as $notification) {
                $notification = (new Notification())
                    ->setDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $notification['date']))
                    ->setChatId((int) $userChatId)
            ;
                $task->addNotification($notification);
                $entityManager->persist($notification);
                $notifications[] = $notification;
            }
        }
        $entityManager->persist($task);
        $entityManager->flush();

        return new JsonResponse($result->setData(['guid' => $task->getGuid()]));
    }

    /**
     * @Route(path="/api/notification/{guid}", methods={"PUT"})
     */
    public function notificationUpdate(Request $request, string $guid, AuthApiService $authApiService, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $result = new InternalResult();

        if (!isset($data['date'])) {
            return new JsonResponse($result->setBadMessage('Date must be provided'));
        }
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $userChatId = $resultOfAuth->getData()['chatId'];

        if (null === $userChatId) {
            return new JsonResponse($result->setBadMessage('First link telegram'));
        }
        /** @var Notification $notification */
        $notification = $entityManager->getRepository(Notification::class)->findOneByGuid($guid);
        $notification
            ->setDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $data['date']))
            ->setChatId((int) $userChatId)
        ;
        $entityManager->flush();

        return new JsonResponse($result->setData([
            'guid' => $notification->getGuid(),
            'date' => $notification->getDate()->format('d.m.Y H:i'),
        ]));
    }

    /**
     * @Route(path="/api/task/{guid}/notification", methods={"POST"})
     */
    public function notificationAdd(Request $request, string $guid, AuthApiService $authApiService, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $result = new InternalResult();

        if (!isset($data['date'])) {
            return new JsonResponse($result->setBadMessage('Date must be provided'));
        }
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $userChatId = $resultOfAuth->getData()['chatId'];

        if (null === $userChatId) {
            return new JsonResponse($result->setBadMessage('First link telegram'));
        }
        /** @var Task $task */
        $task = $entityManager->getRepository(Task::class)->findOneByGuid($guid);
        $notification = new Notification();
        $notification
            ->setDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $data['date']))
            ->setChatId((int) $userChatId)
        ;
        $task->addNotification($notification);
        $entityManager->persist($notification);
        $entityManager->flush();

        return new JsonResponse($result->setData([
            'guid' => $notification->getGuid(),
            'date' => $notification->getDate()->format('d.m.Y H:i'),
        ]));
    }

    /**
     * @Route(path="/api/task/{guid}", methods={"PATCH"})
     */
    public function updateTask(Request $request, string $guid, AuthApiService $authApiService, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $result = new InternalResult();

        if (!isset($data['name'])) {
            return new JsonResponse($result->setBadMessage('Name must be provided'));
        }

        if (!isset($data['startDate']) or !isset($data['endDate'])) {
            return new JsonResponse($result->setBadMessage('Start and End Dates must be provided'));
        }

        if (!isset($data['color'])) {
            return new JsonResponse($result->setBadMessage('Color must be provided'));
        }
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $userGuid = $resultOfAuth->getData()['guid'];
        $task = $entityManager->getRepository(Task::class)->findOneBy(['guid' => $guid]);
        $task->setStartDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $data['startDate']))
            ->setEndDate(\DateTimeImmutable::createFromFormat(DATE_ATOM, $data['endDate']))
            ->setExternalGuid($userGuid)
            ->setName($data['name'])
            ->setColor($data['color'])
        ;

        if (isset($data['description'])) {
            $task->setDescription($data['description']);
        }
        $entityManager->flush();

        return new JsonResponse($result->setData(['guid' => $task->getGuid()]));
    }

    /**
     * @Route(path="/api/task/{guid}", methods={"DELETE"})
     */
    public function deleteTask(Request $request, string $guid, AuthApiService $authApiService, EntityManagerInterface $entityManager): JsonResponse
    {
        $result = new InternalResult();
        $token = $this->fetchTokenFromRequest($request);
        $resultOfAuth = $authApiService->getUser($token);

        if (!$resultOfAuth->isStatus()) {
            return new JsonResponse($result->setBadMessage('Auth failed'));
        }
        $task = $entityManager->getRepository(Task::class)->findOneBy(['guid' => $guid]);

        if (!$task) {
            return new JsonResponse($result->setBadMessage('Task not found'));
        }
        $entityManager->remove($task);
        $entityManager->flush();

        return new JsonResponse($result->setData(['guid' => $task->getGuid()]));
    }
}
