<?php

declare(strict_types=1);

namespace App\Traits;

use Symfony\Component\HttpFoundation\Request;

trait FetchTokenFromHeaderTrait
{
    public function fetchTokenFromRequest(Request $request): string
    {
        $header = $request->headers->get('Authorization');

        if (!$header) {
            return '';
        }
        $exploded = explode(' ', $header);

        return $exploded[1];
    }
}
