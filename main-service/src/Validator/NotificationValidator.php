<?php

declare(strict_types=1);

namespace App\Validator;

use App\Entity\Notification;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NotificationValidator extends ConstraintValidator
{
    /**
     * @param Notification $notification
     */
    public function validate($notification, Constraint $constraint)
    {
        if (null !== $notification->getTask() &&
            $notification->getDate() <= $notification->getTask()->getDate() &&
            new \DateTimeImmutable() <= $notification->getDate() &&
            $constraint instanceof NotificationDateValid) {
            $this->context->buildViolation($constraint->message)
                ->atPath('date')
                ->addViolation()
            ;
        }
    }
}
