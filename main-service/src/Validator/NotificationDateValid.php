<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotificationDateValid extends Constraint
{
    public string $message = 'Вибрано неправильну дату';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
