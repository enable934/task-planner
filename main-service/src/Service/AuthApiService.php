<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\InternalResult;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AuthApiService
{
    private HttpClientInterface $client;

    public function __construct(string $authApiUrl)
    {
        $this->client = HttpClient::createForBaseUri($authApiUrl);
    }

    public function auth(string $username, string $password): InternalResult
    {
        $result = new InternalResult();

        try {
            $response = $this->client->request('POST', '/api/login_check', [
                'json' => [
                    'username' => $username,
                    'password' => $password,
                ],
            ]);
            $response = json_decode($response->getContent(), true);

            if (!isset($response['token'])) {
                throw new \Exception('Auth microservice didn`t respond properly');
            }
        } catch (TransportExceptionInterface $e) {
            return $result->setBadMessage('No connection with auth microservice');
        } catch (\Exception $exception) {
            return $result->setBadMessage('Error. '.$exception->getMessage());
        }

        return $result->setData($response);
    }

    public function getUser(string $token): InternalResult
    {
        $result = new InternalResult();

        try {
            $response = $this->client->request('POST', '/api/current-user', [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                    'Content-Type' => 'application/json',
                ],
            ]);
            $response = json_decode($response->getContent(), true);
        } catch (TransportExceptionInterface $e) {
            return $result->setBadMessage('No connection with auth microservice');
        } catch (\Exception $exception) {
            return $result->setBadMessage('Error. '.$exception->getMessage());
        }

        return $result->setData($response);
    }

    public function register(string $login, string $password): InternalResult
    {
        $result = new InternalResult();

        try {
            $response = $this->client->request('POST', '/api/register', [
                'json' => [
                    'username' => $login,
                    'password' => $password,
                ],
            ]);
            $response = json_decode($response->getContent(), true);

            if (!isset($response['token'])) {
                return $result->setBadMessage($response['message']);
            }
        } catch (TransportExceptionInterface $e) {
            return $result->setBadMessage('No connection with auth microservice');
        } catch (\Exception $exception) {
            return $result->setBadMessage('Error. '.$exception->getMessage());
        }

        return $result->setData($response);
    }
}
