Feature: Register
  In order to sign in
  As a web user
  I need to be able to register

  @javascript
  Scenario: Log in with user which exists
    Given I am on homepage
    When I follow "→ register now"
    Then I should see "Registration"
    When I fill in "username" with "user_test"
    And I fill in "password" with "123"
    And I fill in "confirm" with "123"
    And I press "register"
    And I wait for 3 sec
    Then I should see "Main page"
    And I click on ".ant-dropdown-trigger"
    And I follow "logout"