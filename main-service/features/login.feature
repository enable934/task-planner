Feature: Login
  In order to login in
  As a web user
  I need to be able to log in

  @javascript
  Scenario: Log in with user which exists
    Given I am on homepage
    When I fill in "username" with "user"
    And I fill in "password" with "123"
    And I press "log in"
    And I wait for 3 sec
    Then I should see "Main page"
    And I click on ".ant-dropdown-trigger"
    And I follow "logout"

  @javascript
  Scenario: Log in with user which doesn't exists
    Given I am on homepage
    When I fill in "username" with "user"
    And I fill in "password" with "1234"
    And I press "log in"
    And I wait for 3 sec
    Then I should see "Error. HTTP/1.1 401 Unauthorized returned for"