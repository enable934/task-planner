<?php

declare(strict_types=1);

use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * waitMinutes.
     *
     * @When /^(?:|I )wait (?:|for )(?P<time>\d+) sec?$/
     *
     * @param int $time number of seconds to wait
     *
     * @return FeatureContext
     */
    public function waitSeconds(int $time): self
    {
        sleep($time);

        return $this;
    }

    /**
     * Presses button with specified id|name|title|alt|value
     * Example: When I click on ".ant-dropdown-trigger"
     * Example: And I click on "Log In".
     *
     * @When /^(?:|I )click on "(?P<element>(?:[^"]|\\")*)"$/
     */
    public function clickOnElement(string $element)
    {
        $element = $this->fixStepArgument($element);
        $this->getSession()->getPage()->find('css', $element)->click();
    }
}
