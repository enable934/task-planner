<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201206021114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE notification ALTER date TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE notification ALTER date DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN notification.date IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE task ALTER start_date TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE task ALTER start_date DROP DEFAULT');
        $this->addSql('ALTER TABLE task ALTER end_date TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE task ALTER end_date DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN task.start_date IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN task.end_date IS \'(DC2Type:datetimetz_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task ALTER start_date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE task ALTER start_date DROP DEFAULT');
        $this->addSql('ALTER TABLE task ALTER end_date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE task ALTER end_date DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN task.start_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN task.end_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE notification ALTER date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE notification ALTER date DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN notification.date IS \'(DC2Type:datetime_immutable)\'');
    }
}
