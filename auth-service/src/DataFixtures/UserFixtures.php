<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const USERNAME_USER = 'user';
    public const USERNAME_USER2 = 'user2';
    public const PASSWORD = '123';
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user
            ->setUsername(self::USERNAME_USER)
            ->setSalt(self::PASSWORD)
        ;
        $user2 = new User();
        $user2
            ->setUsername(self::USERNAME_USER2)
            ->setSalt(self::PASSWORD)
        ;
        $user->setPassword($this->passwordEncoder->encodePassword($user, self::PASSWORD));
        $user2->setPassword($this->passwordEncoder->encodePassword($user2, self::PASSWORD));
        $manager->persist($user);
        $manager->persist($user2);
        $manager->flush();
    }
}
