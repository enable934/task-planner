<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MainController extends AbstractController
{
    /**
     * @Route(path="/api/register", methods={"POST"})
     */
    public function registerUser(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, JWTTokenManagerInterface $JWTTokenManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['username'])) {
            return new JsonResponse(['status' => false, 'message' => 'Key \'username\' must be provided']);
        }

        if (!isset($data['password'])) {
            return new JsonResponse(['status' => false, 'message' => 'Key \'password\' must be provided']);
        }
        /** @var null|User $userExists */
        $userExists = $entityManager->getRepository(User::class)->findOneByUsername($data['username']);

        if (null !== $userExists) {
            return new JsonResponse(['status' => false, 'message' => 'This username is used by someone else']);
        }
        $user = new User();
        $user->setUsername($data['username'])
            ->setSalt(Uuid::uuid4()->toString())
            ->setPassword($passwordEncoder->encodePassword($user, $data['password']))
        ;
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse(['token' => $JWTTokenManager->create($user)]);
    }

    /**
     * @Route(path="/api/current-user")
     */
    public function getUserInfo(TokenStorageInterface $storage): JsonResponse
    {
        /** @var User $user */
        $user = $storage->getToken()->getUser();

        return new JsonResponse($user);
    }
}
