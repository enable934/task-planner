<?php

declare(strict_types=1);

namespace App\Traits;

trait StringHelperTrait
{
    public function getGuidFromMessage(string $messageText): string
    {
        return trim(mb_substr($messageText, mb_strlen('/start')));
    }
}
