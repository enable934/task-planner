<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Traits\StringHelperTrait;
use Borsaco\TelegramBotApiBundle\Service\Bot;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LinkTelegram extends Command
{
    use StringHelperTrait;

    protected static $defaultName = 'app:link';

    private Bot $bot;

    private EntityManagerInterface $entityManager;

    public function __construct(Bot $bot, EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->bot = $bot;
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bot = $this->bot->getBot('first');
        $updates = $bot->getUpdates();
        $users = $this->entityManager->getRepository(User::class)->findAll();

        foreach ($updates as $update) {
            $messageText = $update->getMessage()->get('text');
            /** @var User $user */
            foreach ($users as $user) {
                if (null !== $messageText && $this->getGuidFromMessage($messageText) === $user->getUuid()) {
                    $user->setChatId((int) $update->getMessage()->get('chat')->get('id'));
                    $output->writeln('Successful linked user \''.$user->getUsername().'\' with chat_id='.$user->getChatId());
                }
            }
        }
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
