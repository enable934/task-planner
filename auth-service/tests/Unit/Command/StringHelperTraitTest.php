<?php

declare(strict_types=1);

namespace App\Tests\Unit\Command;

use App\Traits\StringHelperTrait;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class StringHelperTraitTest extends TestCase
{
    use StringHelperTrait;

    public function testProperExtraction()
    {
        $expected = '123';
        $result = $this->getGuidFromMessage('/start 123');

        $this->assertSame($expected, $result);
    }

    public function testBadMessage()
    {
        $expected = '';
        $result = $this->getGuidFromMessage('123');

        $this->assertSame($expected, $result);
    }
}
