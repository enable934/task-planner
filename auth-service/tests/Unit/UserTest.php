<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class UserTest extends TestCase
{
    public function testUserWithNotSettedChatIdToJson()
    {
        $user = new User();
        $user->setUsername('test');

        $result = json_encode($user);

        $this->assertJson($result);

        $resultDecoded = json_decode($result, true);

        $this->assertArrayHasKey('guid', $resultDecoded);
        $this->assertArrayHasKey('chatId', $resultDecoded);
        $this->assertNull($resultDecoded['chatId']);
    }

    public function testUserWithNotSettedGuidToJson()
    {
        $user = new User();
        $user->setUsername('test');

        $result = json_encode($user);

        $this->assertJson($result);

        $resultDecoded = json_decode($result, true);

        $this->assertArrayHasKey('guid', $resultDecoded);
        $this->assertArrayHasKey('chatId', $resultDecoded);
    }

    public function testUserWithNotSettedUsernameToJson()
    {
        $user = new User();

        $result = json_encode($user);

        $this->assertJson($result);

        $resultDecoded = json_decode($result, true);

        $this->assertArrayHasKey('guid', $resultDecoded);
        $this->assertArrayHasKey('chatId', $resultDecoded);
        $this->assertEmpty($resultDecoded['username']);
    }

    public function testUserToJson()
    {
        $user = new User();
        $user->setChatId(12314)
            ->setUsername('test')
        ;

        $result = json_encode($user);

        $this->assertJson($result);

        $resultDecoded = json_decode($result, true);

        $this->assertArrayHasKey('guid', $resultDecoded);
        $this->assertArrayHasKey('chatId', $resultDecoded);
    }
}
