<?php

declare(strict_types=1);

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class IntegrationTestCase extends WebTestCase
{
    protected KernelBrowser $client;
    protected EntityManagerInterface $em;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->enableProfiler();
        $this->client->disableReboot();
        $this->em = $this->client->getContainer()
            ->get('doctrine.orm.entity_manager')
        ;
        $this->em->beginTransaction();
        $this->em->getConnection()->setAutoCommit(false);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if ($this->em->getConnection()->isTransactionActive()) {
            $this->em->rollback();
        }
    }

    protected function makeRequest(string $username, string $url, string $method, array $content = []): Response
    {
        $token = $this->getJWTToken($username);
        $this->client->request(
            $method,
            $url,
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'Bearer '.$token],
            json_encode($content)
        );

        return $this->client->getResponse();
    }

    protected function getJWTToken(string $username): string
    {
        $data = ['username' => $username, 'roles' => []];

        return (string) $this->client->getContainer()
            ->get('lexik_jwt_authentication.encoder')
            ->encode($data)
        ;
    }
}
