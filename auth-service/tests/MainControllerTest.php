<?php

declare(strict_types=1);

namespace App\Tests;

/**
 * @internal
 * @coversNothing
 */
class MainControllerTest extends IntegrationTestCase
{
    public function testRegister(): void
    {
        $this->client->request('POST', '/api/register', [], [], [], json_encode(['username' => 'user_test', 'password' => '1234']));
        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();

        $responseDecoded = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('token', $responseDecoded);
    }
}
