<?php

declare(strict_types=1);

namespace App\Tests;

use App\DataFixtures\UserFixtures;

/**
 * @internal
 * @coversNothing
 */
class AuthTest extends IntegrationTestCase
{
    public function testCheckPermissions()
    {
        $response = $this->makeRequest(UserFixtures::USERNAME_USER, '/api/current-user', 'GET');

        $this->assertIsString($response->getContent());
        $this->assertResponseIsSuccessful();
    }

    public function testCheckPermissionsWithBadUsername()
    {
        $this->makeRequest('bad_username', '/api/current-user', 'GET');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testAuth()
    {
        $this->client->request('POST', '/api/login_check', [], [], [
            'CONTENT_TYPE' => 'application/json',
        ], json_encode(['username' => UserFixtures::USERNAME_USER, 'password' => UserFixtures::PASSWORD]));
        $response = $this->client->getResponse();
        $this->assertResponseIsSuccessful();
        $this->assertJson($response->getContent());
        $this->assertArrayHasKey('token', json_decode($response->getContent(), true));
    }
}
