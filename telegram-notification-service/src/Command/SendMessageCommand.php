<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\TelegramService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMessageCommand extends Command
{
    protected static $defaultName = 'app:send';

    private TelegramService $telegramService;

    public function __construct(TelegramService $telegramService, string $name = null)
    {
        parent::__construct($name);
        $this->telegramService = $telegramService;
    }

    protected function configure()
    {
        $this->addArgument('text', InputArgument::REQUIRED);
        $this->addArgument('chatId', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->telegramService->sendMessage((int) $input->getArgument('chatId'), $input->getArgument('text'));

        return Command::SUCCESS;
    }
}
