<?php

declare(strict_types=1);

namespace App\Service;

use Borsaco\TelegramBotApiBundle\Service\Bot;

class TelegramService
{
    private Bot $bot;

    public function __construct(Bot $bot)
    {
        $this->bot = $bot;
    }

    public function sendMessage(int $chatId, string $text): void
    {
        $bot = $this->bot->getBot('first');
        $bot->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
        ]);
    }
}
