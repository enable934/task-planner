<?php

declare(strict_types=1);

namespace App\Messenger\Serializer;

use App\Message\NotificationMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

class JsonMessageSerializer implements SerializerInterface
{
    public function decode(array $encodedEnvelope): Envelope
    {
        $body = json_decode($encodedEnvelope['body'], true);
        $message = new NotificationMessage($body['taskName'], $body['taskDescription'], (int) $body['chatId']);
        $stamps = [];

        return new Envelope($message, $stamps);
    }

    public function encode(Envelope $envelope): array
    {
        return [];
    }
}
