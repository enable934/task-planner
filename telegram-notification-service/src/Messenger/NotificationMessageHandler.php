<?php

declare(strict_types=1);

namespace App\Messenger;

use App\Entity\Log;
use App\Message\NotificationMessage;
use App\Service\TelegramService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class NotificationMessageHandler implements MessageHandlerInterface
{
    private TelegramService $telegramService;

    private EntityManagerInterface $entityManager;

    public function __construct(TelegramService $telegramService, EntityManagerInterface $entityManager)
    {
        $this->telegramService = $telegramService;
        $this->entityManager = $entityManager;
    }

    public function __invoke(NotificationMessage $notificationMessage)
    {
        $this->telegramService->sendMessage(
            $notificationMessage->getChatId(),
            sprintf('Notification: %s. Description: %s', $notificationMessage->getTaskName(), $notificationMessage->getTaskDescription())
        );
        $log = new Log();
        $log->setChatId($notificationMessage->getChatId())
            ->setDate(new \DateTimeImmutable())
            ->setText(sprintf('Text: %s. Description: %s', $notificationMessage->getTaskName(), $notificationMessage->getTaskDescription()))
        ;
        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }
}
