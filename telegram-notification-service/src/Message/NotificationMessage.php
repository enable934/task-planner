<?php

declare(strict_types=1);

namespace App\Message;

class NotificationMessage
{
    private string $taskName;
    private string $taskDescription;
    private int $chatId;

    public function __construct(string $taskName, string $taskDescription, int $chatId)
    {
        $this->taskName = $taskName;
        $this->taskDescription = $taskDescription;
        $this->chatId = $chatId;
    }

    public function getTaskName(): string
    {
        return $this->taskName;
    }

    public function getTaskDescription(): string
    {
        return $this->taskDescription;
    }

    public function getChatId(): int
    {
        return $this->chatId;
    }
}
